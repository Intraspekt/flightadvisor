package htec.com.service.exceptions;

public class ServiceException extends RuntimeException {
    public ServiceException(String messageCode) {
        super(messageCode);
    }
}
