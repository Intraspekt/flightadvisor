package htec.com.service.exceptions;

public class ValidationException extends RuntimeException {
    public ValidationException(String messageCode) {
        super(messageCode);
    }
}
