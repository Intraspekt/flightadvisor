package htec.com.service.helpers;

import htec.com.bom.entities.Airport;
import htec.com.service.calcEngine.models.AirportModel;

public class DistanceHelper {
    public static double calculateDistance(Airport source, Airport destination){
        return calculateDistance(source.getLatitude(), source.getLongitude(), destination.getLatitude(), destination.getLongitude());
    }

    public static double calculateDistance(AirportModel source, AirportModel destination){
        return calculateDistance(source.getLatitude(), source.getLongitude(), destination.getLatitude(), destination.getLongitude());
    }

    public static double calculateDistance(Double sourceLatitude, Double sourceLongitude, Double destinationLatitude, Double destinationLongitude){
        double R = 6372.8; // Earth's Radius, in kilometers

        double dLat = Math.toRadians(destinationLatitude - sourceLatitude);
        double dLon = Math.toRadians(destinationLongitude - sourceLongitude);
        double lat1 = Math.toRadians(sourceLatitude);
        double lat2 = Math.toRadians(destinationLatitude);

        double a = Math.pow(Math.sin(dLat / 2),2)
                + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));

        return R * c;
    }
}
