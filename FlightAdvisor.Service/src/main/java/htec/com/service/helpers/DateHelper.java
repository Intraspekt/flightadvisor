package htec.com.service.helpers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateHelper {
    public static DateTimeFormatter FORMATER = DateTimeFormatter.ofPattern("dd.mm.yyy HH:mm:ss");

    public static String formatDate(LocalDateTime dateTime){
        return dateTime.format(FORMATER);
    }
}
