package htec.com.service.helpers;

public class FormatHelper {
    public static String roundTo2Decimals(double value){
        return String.format("%.2f", value);
    }
}
