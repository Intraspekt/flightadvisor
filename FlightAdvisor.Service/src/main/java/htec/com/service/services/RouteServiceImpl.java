package htec.com.service.services;

import htec.com.bom.entities.Airport;
import htec.com.bom.entities.Route;
import htec.com.dal.repositories.AirportRepository;
import htec.com.dal.repositories.RouteRepository;
import htec.com.service.calcEngine.Graph;
import htec.com.service.dtos.CheapestRoutesResult;
import htec.com.service.services.interfaces.RouteService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class RouteServiceImpl implements RouteService {
    private final static String IMPORT_FILE_PATH = "../FlightAdvisor.Service/src/main/resources/importData/routes.txt";
    private final AirportRepository airportRepository;
    private final RouteRepository routeRepository;
    private final Graph graph;

    @Override
    public void importData() {
        routeRepository.deleteAllInBatch();
        List<Airport> airports = airportRepository.findAll();
        Map<Long, Airport> airportsById = airports.stream()
                .collect(Collectors.toMap(Airport::getId, Function.identity()));

        try (Stream<String> stream = Files.lines(Paths.get(IMPORT_FILE_PATH))) {
            List<Route> routesToAdd = stream.map(x -> processLine(x, airports, airportsById))
                    .filter(x -> x != null).collect(Collectors.toList());

            if(CollectionUtils.isNotEmpty(routesToAdd)){
                routeRepository.saveAll(routesToAdd);
                graph.initializeNodes(airports, routesToAdd);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public CheapestRoutesResult findCheapestRoutes(long sourceCityId, long destinationCityId){
        return graph.findCheapestRoutes(sourceCityId, destinationCityId);
    }

    private Route processLine(String line,  List<Airport> airports, Map<Long, Airport> airportsById){
        Route route = null;
        String[] fields = line.split(",");
        Long sourceId = parseLong(fields[3]);
        Long destinationId = parseLong(fields[5]);
        Airport sourceAirport = sourceId != null ? airportsById.get(sourceId) : getAirportByCode(fields[2], airports);
        Airport destinationAirport = destinationId != null ? airportsById.get(destinationId) : getAirportByCode(fields[4], airports);

        if(sourceAirport != null && destinationAirport != null){
            route = new Route(fields[0], parseLong(fields[1]), sourceAirport, destinationAirport, fields[6].equals("Y"),
                    Integer.parseInt(fields[7]), fields[8], Double.parseDouble(fields[9]));
        }

        return route;
    }

    private Long parseLong(String s){
        Long value = null;

        try {
            value = Long.parseLong(s);
        }
        catch (Exception e) { }

        return value;
    }


    private Airport getAirportByCode(String code, List<Airport> airports){
        Optional<Airport> airport = code.length() == 3
                ? airports.stream().filter(x -> x.getIATA() != null && x.getIATA().equals(code)).findFirst()
                : airports.stream().filter(x -> x.getICAO() != null && x.getICAO().equals(code)).findFirst();

        return airport.orElse(null);
    }
}
