package htec.com.service.services.interfaces;

import htec.com.service.dtos.CheapestRoutesResult;

public interface RouteService {
    void importData();
    CheapestRoutesResult findCheapestRoutes(long sourceCityId, long destinationCityId);
}
