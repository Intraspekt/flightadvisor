package htec.com.service.services;

import htec.com.bom.entities.Airport;
import htec.com.bom.entities.City;
import htec.com.dal.repositories.AirportRepository;
import htec.com.dal.repositories.CityRepository;
import htec.com.dal.repositories.RouteRepository;
import htec.com.service.services.interfaces.AirportService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class AirportServiceImpl implements AirportService {
    private final static String UNDEFINED = "\\N";
    private final static String IMPORT_FILE_PATH = "../FlightAdvisor.Service/src/main/resources/importData/airports.txt";
    private final CityRepository cityRepository;
    private final AirportRepository airportRepository;
    private final RouteRepository routeRepository;

    @Override
    public void importData() {
        routeRepository.deleteAllInBatch();
        airportRepository.deleteAllInBatch();

        Map<String, City> cities = cityRepository.findAll().stream()
                .collect(Collectors.toMap(x -> x.getName().toLowerCase(), Function.identity()));

        try (Stream<String> stream = Files.lines(Paths.get(IMPORT_FILE_PATH))) {
            List<Airport> airportsToAdd = stream.map(x -> processLine(x, cities))
                    .filter(x -> x != null).collect(Collectors.toList());

            if(CollectionUtils.isNotEmpty(airportsToAdd)){
                airportRepository.saveAll(airportsToAdd);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Airport processLine(String line,  Map<String, City> cities){
        Airport airport = null;
        String[] fields = line.split(",");

        if(fields.length != 14){
            return null;
        }

        String cityName = formatString(fields[2]);
        if(StringUtils.isEmpty(cityName)){
            return null;
        }

        City city = cities.get(cityName.toLowerCase());
        
        if(city != null) {
            airport = new Airport(Long.parseLong(fields[0]), formatString(fields[1]), city, formatString(fields[3]), formatCode(fields[4]), formatString(fields[5]),
                    Double.parseDouble(fields[6]), Double.parseDouble(fields[7]), Double.parseDouble(fields[8]), formatTimeZoneOffset(fields[9]),
                    formatDST(fields[10]), formatCode(fields[11]), formatString(fields[12]), formatString(fields[13]));
        }

        return airport;
    }

    private String formatString(String value){
        return value.substring(1, value.length() - 1);
    }

    private Character formatDST(String dst){
        return dst.equals(UNDEFINED)
                ? null
                : formatString(dst).charAt(0);
    }

    private String formatCode(String value){
        return value.equals(UNDEFINED)
                ? null
                : formatString(value);
    }

    private Double formatTimeZoneOffset(String value){
        return value.equals(UNDEFINED)
                ? null
                : Double.parseDouble(value);
    }
}
