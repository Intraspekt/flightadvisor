package htec.com.service.services.interfaces;

import htec.com.service.dtos.UserDto;

public interface RegistrationService {
    void registerUser(UserDto userDto);
}
