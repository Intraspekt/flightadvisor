package htec.com.service.services.interfaces;

import htec.com.service.dtos.CityDto;

import java.util.List;

public interface CityService {
    List<CityDto> get(String filterName, Integer numberOfComments);
    CityDto create(CityDto cityDto);
}
