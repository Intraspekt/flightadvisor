package htec.com.service.services;

import htec.com.bom.entities.UserRole;
import htec.com.bom.entities.User;
import htec.com.bom.enums.UserRoleEnum;
import htec.com.dal.repositories.RoleRepository;
import htec.com.dal.repositories.UserRepository;
import htec.com.service.dtos.UserDto;
import htec.com.service.exceptions.ValidationException;
import htec.com.service.services.interfaces.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class RegistrationServiceImpl implements RegistrationService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void registerUser(UserDto userDto) {
        User user = userRepository.findByUsernameIgnoreCase(userDto.getUsername());
        String password = passwordEncoder.encode(userDto.getPassword());
        if(user == null){
            UserRole userRole = roleRepository.findByName(UserRoleEnum.BASIC_USER.toString());
            User newUser = new User(userDto.getFirstName(), userDto.getLastName(), userDto.getUsername(), password, Set.of(userRole));
            userRepository.save(newUser);
        } else{
            throw new ValidationException("usernameAlreadyExists");
        }
    }
}
