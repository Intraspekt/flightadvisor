package htec.com.service.services;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ModelConverter {
    private final ConversionService conversionService;

    public <S, T> List<T> convert(final List<S> sourceObjects, final Class<T> targetType){
        return sourceObjects != null
                ? sourceObjects.stream().map(x -> conversionService.convert(x, targetType)).collect(Collectors.toList())
                : null;
    }

    public <T> T convert(final Object sourceObject, final Class<T> targetType) {
        T targetObject;

        targetObject = conversionService.convert(sourceObject, targetType);

        return targetObject;
    }
}
