package htec.com.service.services;

import htec.com.bom.entities.City;
import htec.com.dal.repositories.CityRepository;
import htec.com.service.dtos.CityDto;
import htec.com.service.dtos.CommentDto;
import htec.com.service.services.interfaces.CityService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CityServiceImpl implements CityService {
    private final CityRepository cityRepository;
    private final ModelConverter mapper;

    @Override
    public List<CityDto> get(String filterName, Integer numberOfComments) {
        List<City> cities = StringUtils.isNotEmpty(filterName)
                ? cityRepository.getByNameContainsIgnoreCase(filterName)
                : cityRepository.findAll();

        List<CityDto> cityDtos = numberOfComments == null || numberOfComments < 0
                ? mapper.convert(cities, CityDto.class)
                : mapCitiesWithNumberOfComments(cities, numberOfComments);

        return cityDtos;
    }

    @Override
    public CityDto create(CityDto cityDto) {
        City city = mapper.convert(cityDto, City.class);
        cityRepository.save(city);
        cityDto.setId(city.getId());

        return cityDto;
    }

    private List<CityDto> mapCitiesWithNumberOfComments(List<City> cities, Integer numberOfComments){
        List<CityDto> dtos = cities.stream().map(city -> {
            var cityDto = new CityDto();
            cityDto.setId(city.getId());
            cityDto.setCountry(city.getCountry());
            cityDto.setDescription(city.getDescription());
            cityDto.setName(city.getName());

            if(city.getComments() != null) {
                cityDto.setComments(city.getComments().stream().limit(numberOfComments)
                        .map(x -> mapper.convert(x, CommentDto.class)).collect(Collectors.toList()));
            }
            return cityDto;
        }).collect(Collectors.toList());

        return dtos;
    }
}
