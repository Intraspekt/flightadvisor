package htec.com.service.services.interfaces;

import htec.com.service.dtos.CommentDto;

public interface CommentService {
    CommentDto create(CommentDto dto);
    CommentDto edit(Long id, CommentDto dto);
    void delete(Long id);
}
