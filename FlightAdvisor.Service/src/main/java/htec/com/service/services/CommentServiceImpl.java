package htec.com.service.services;

import htec.com.bom.entities.Comment;
import htec.com.dal.repositories.CommentRepository;
import htec.com.service.dtos.CommentDto;
import htec.com.service.services.interfaces.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final ModelConverter mapper;

    @Override
    public CommentDto create(CommentDto dto) {
        dto.setId(null);
        Comment comment = mapper.convert(dto, Comment.class);
        commentRepository.save(comment);

        return mapper.convert(comment, CommentDto.class);
    }

    @Override
    public CommentDto edit(Long id, CommentDto dto) {
       Comment comment = commentRepository.getOne(id);
       if(!canManageComment(comment)){
           throw new ValidationException("notAllowedToEditComment");
       }

        comment.setText(dto.getText());
        commentRepository.save(comment);

        return mapper.convert(comment, CommentDto.class);
    }

    @Override
    public void delete(Long id) {
        Comment comment = commentRepository.getOne(id);
        if(!canManageComment(comment)){
            throw new ValidationException("notAllowedToDeleteComment");
        }

        commentRepository.deleteById(id);
    }

    private String getLoggedUserName(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    private boolean canManageComment(Comment comment){
        return comment.getUser().equals(getLoggedUserName());
    }
}
