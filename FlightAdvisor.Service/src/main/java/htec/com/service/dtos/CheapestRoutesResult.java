package htec.com.service.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheapestRoutesResult {
    private List<RouteDto> routes;
    private String totalPrice;
    private String length;
}
