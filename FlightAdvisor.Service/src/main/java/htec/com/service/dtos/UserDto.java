package htec.com.service.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UserDto {
    private Integer id;

    @NotEmpty(message = "firstName.required")
    private String firstName;

    @NotEmpty(message = "lastName.required")
    private String lastName;

    @NotEmpty(message = "username.required")
    private String username;

    @NotEmpty(message = "password.required")
    private String password;
}
