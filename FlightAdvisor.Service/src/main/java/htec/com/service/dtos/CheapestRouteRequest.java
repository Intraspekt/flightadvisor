package htec.com.service.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CheapestRouteRequest {

    @NotNull(message = "sourceCityId.required")
    private Long sourceCityId;

    @NotNull(message = "destinationCityId.required")
    private Long destinationCityId;
}
