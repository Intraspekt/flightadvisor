package htec.com.service.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CityDto {
    private Long id;

    @NotEmpty(message = "name.required")
    private String name;

    @NotEmpty(message = "description.required")
    private String description;

    @NotEmpty(message = "country.required")
    private String country;

    private List<CommentDto> comments;

    public CityDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
