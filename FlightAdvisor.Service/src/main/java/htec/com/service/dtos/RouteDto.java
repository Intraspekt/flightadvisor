package htec.com.service.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import htec.com.service.calcEngine.models.RouteNode;
import htec.com.service.helpers.FormatHelper;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RouteDto {

    public RouteDto(RouteNode route){
        sourceAirport = route.getPrevious().getName();
        sourceCity = route.getPrevious().getCity();
        destinationAirport = route.getCurrent().getName();
        destinationCity = route.getCurrent().getCity();
        price = FormatHelper.roundTo2Decimals(route.getCurrentRoutePrice());
    }

    private String sourceAirport;
    private CityDto sourceCity;
    private String destinationAirport;
    private CityDto destinationCity;
    private String price;
}
