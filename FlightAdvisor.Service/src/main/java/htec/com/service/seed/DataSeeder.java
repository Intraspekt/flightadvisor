package htec.com.service.seed;

import htec.com.bom.entities.City;
import htec.com.bom.entities.User;
import htec.com.bom.entities.UserRole;
import htec.com.bom.enums.UserRoleEnum;
import htec.com.dal.repositories.CityRepository;
import htec.com.dal.repositories.RoleRepository;
import htec.com.dal.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class DataSeeder implements ApplicationRunner {

    private static final String ADMIN = "Admin";
    private static final String ADMIN_PASSWORD = "ztonpk";

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Transactional
    public void run(ApplicationArguments args) {
        seedRoles();
        seedAdmin();
    }

    private void seedRoles(){
        Arrays.stream(UserRoleEnum.values()).forEach(x -> {
            if(!roleRepository.existsByName(x.toString())){
                roleRepository.save(new UserRole(x.toString()));
            }
        });
    }

    private void seedAdmin(){
        if(!userRepository.existsByUsername(ADMIN)){
            List<UserRole> adminRoles = roleRepository.findAll();
            String encodedPassword = passwordEncoder.encode(ADMIN_PASSWORD);
            User adminUser = new User(StringUtils.EMPTY, StringUtils.EMPTY, ADMIN, encodedPassword, new HashSet<>(adminRoles));
            userRepository.save(adminUser);
        }
    }
}