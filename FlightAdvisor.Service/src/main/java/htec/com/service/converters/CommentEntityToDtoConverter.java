package htec.com.service.converters;

import htec.com.bom.entities.Comment;
import htec.com.service.dtos.CommentDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class CommentEntityToDtoConverter implements Converter<Comment, CommentDto> {
    @Override
    public CommentDto convert(Comment comment) {
        var commentDto = new CommentDto();
        commentDto.setId(comment.getId());
        commentDto.setText(comment.getText());
        commentDto.setCityId(comment.getCity().getId());
        commentDto.setCreatedOn(comment.getCreatedOn());
        commentDto.setUpdatedOn(comment.getUpdatedOn());
        commentDto.setUser(comment.getUser());

        return commentDto;
    }
}
