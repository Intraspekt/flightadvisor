package htec.com.service.converters;

import htec.com.bom.entities.Comment;
import htec.com.dal.repositories.CityRepository;
import htec.com.service.dtos.CommentDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CommentDtoToEntityConverter implements Converter<CommentDto, Comment> {
    private final CityRepository cityRepository;

    @Override
    public Comment convert(CommentDto commentDto) {
        var comment = new Comment();
        comment.setId(commentDto.getId());
        comment.setText(commentDto.getText());
        comment.setCity(cityRepository.getOne(commentDto.getCityId()));

        return comment;
    }
}
