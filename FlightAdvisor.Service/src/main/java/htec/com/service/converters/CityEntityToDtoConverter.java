package htec.com.service.converters;

import htec.com.bom.entities.City;
import htec.com.service.dtos.CityDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CityEntityToDtoConverter implements Converter<City, CityDto> {
    private final CommentEntityToDtoConverter commentMapper;

    @Override
    public CityDto convert(City city) {
        var cityDto = new CityDto();
        cityDto.setId(city.getId());
        cityDto.setCountry(city.getCountry());
        cityDto.setDescription(city.getDescription());
        cityDto.setName(city.getName());

        if(city.getComments() != null) {
            cityDto.setComments(city.getComments().stream().map(x -> commentMapper.convert(x)).collect(Collectors.toList()));
        }

        return cityDto;
    }
}
