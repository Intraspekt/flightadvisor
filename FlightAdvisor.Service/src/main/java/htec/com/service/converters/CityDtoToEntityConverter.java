package htec.com.service.converters;

import htec.com.bom.entities.City;
import htec.com.service.dtos.CityDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class CityDtoToEntityConverter implements Converter<CityDto, City> {
    @Override
    public City convert(CityDto cityDto) {
        var city = new City();

        city.setCountry(cityDto.getCountry());
        city.setDescription(cityDto.getDescription());
        city.setName(cityDto.getName());

        return city;
    }
}
