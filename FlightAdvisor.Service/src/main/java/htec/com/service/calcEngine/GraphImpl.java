package htec.com.service.calcEngine;

import htec.com.bom.entities.Airport;
import htec.com.bom.entities.Route;
import htec.com.dal.repositories.AirportRepository;
import htec.com.dal.repositories.RouteRepository;
import htec.com.service.calcEngine.models.AirportModel;
import htec.com.service.calcEngine.models.RouteModel;
import htec.com.service.dtos.CheapestRoutesResult;
import htec.com.service.dtos.CityDto;
import htec.com.service.exceptions.ServiceException;
import htec.com.service.helpers.DistanceHelper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class GraphImpl implements Graph {
    private Map<Long, AirportModel> nodes;

    public GraphImpl(AirportRepository airportRepository, RouteRepository routeRepository) {
       var airports = airportRepository.findAll();
       var routes = routeRepository.findAll();

       if(!routes.isEmpty()) {
           initializeNodes(airports, routes);
       }
    }

    public void initializeNodes(List<Airport> airports, List<Route> routes){
        List<Route> emptyRouteList = List.of();
        Map<Airport, List<Route>> routesFromAirports = routes.stream().collect(Collectors.groupingBy(Route::getSource, Collectors.toList()));
        List<AirportModel> models = routesFromAirports.entrySet().stream().map(x -> createAirportModel(x.getKey(), x.getValue())).collect(Collectors.toList());
        nodes = models.stream().collect(Collectors.toMap(x -> x.getId(), Function.identity()));
        Map<Long, AirportModel> airportsWithoutRoutes = airports.stream().filter(x -> !nodes.keySet().contains(x.getId())).map(x -> createAirportModel(x, emptyRouteList))
                .collect(Collectors.toMap(x -> x.getId(), Function.identity()));
        nodes.putAll(airportsWithoutRoutes);
    }

    public CheapestRoutesResult findCheapestRoutes(long sourceCityId, long destinationCityId){
        List<AirportModel> startNodes = nodes.values().stream().filter(x -> x.getCity().getId().equals(sourceCityId)).collect(Collectors.toList());
        List<AirportModel> endNodes = nodes.values().stream().filter(x -> x.getCity().getId().equals(destinationCityId)).collect(Collectors.toList());

        if(startNodes.isEmpty()){
            throw new ServiceException("noAirportsInSourceCity");
        }

        if(endNodes.isEmpty()){
            throw new ServiceException("noAirportsInDestinationCity");
        }

        RouteFinder routeFinder = new RouteFinder(startNodes, endNodes, nodes);
        CheapestRoutesResult result = routeFinder.execute();

        return result;
    }

    private AirportModel createAirportModel(Airport airport, List<Route> routes){
        var model = new AirportModel();
        model.setId(airport.getId());
        model.setName(airport.getName());
        model.setCity(new CityDto(airport.getCity().getId(), airport.getCity().getName()));
        model.setLongitude(airport.getLongitude());
        model.setLatitude(airport.getLatitude());

        model.setRoutes(routes.stream().map(this::createRouteModel).collect(Collectors.toList()));

        return model;
    }

    private RouteModel createRouteModel(Route route){
        var routeModel = new RouteModel();
        routeModel.setSourceId(route.getSource().getId());
        routeModel.setDestinationId(route.getDestination().getId());
        routeModel.setPrice(route.getPrice());
        routeModel.setDistance(DistanceHelper.calculateDistance(route.getSource(), route.getDestination()));

        return routeModel;
    }
}
