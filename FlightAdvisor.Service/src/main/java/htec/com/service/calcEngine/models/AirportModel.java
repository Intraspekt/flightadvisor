package htec.com.service.calcEngine.models;

import htec.com.service.dtos.CityDto;
import lombok.Data;

import java.util.List;

@Data
public class AirportModel {
    private Long id;
    private String name;
    private CityDto city;
    private Double longitude;
    private Double latitude;

    private List<RouteModel> routes;
}
