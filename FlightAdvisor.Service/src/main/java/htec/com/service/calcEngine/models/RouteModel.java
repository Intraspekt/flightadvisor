package htec.com.service.calcEngine.models;

import lombok.Data;

@Data
public class RouteModel {
    private Long sourceId;
    private Long destinationId;
    private Double price;
    private Double distance;
}
