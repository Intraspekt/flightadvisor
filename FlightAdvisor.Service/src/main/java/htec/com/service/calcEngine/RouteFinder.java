package htec.com.service.calcEngine;

import htec.com.service.calcEngine.models.AirportModel;
import htec.com.service.calcEngine.models.RouteNode;
import htec.com.service.dtos.CheapestRoutesResult;
import htec.com.service.dtos.RouteDto;
import htec.com.service.helpers.DistanceHelper;
import htec.com.service.helpers.FormatHelper;

import java.util.*;

public class RouteFinder {
    private Queue<RouteNode> openSet = new PriorityQueue<>();
    Map<AirportModel, RouteNode> allNodes = new HashMap<>();
    Map<Long, AirportModel> nodesPerId;
    private final long destinationCityId;
    private final AirportModel endPoint;

    public RouteFinder(List<AirportModel> starts, List<AirportModel> ends, Map<Long, AirportModel> nodesPerId){
        this.nodesPerId = nodesPerId;
        this.destinationCityId = ends.get(0).getCity().getId();
        endPoint = ends.get(0);

        for(var airportModel: starts){
            var routeNode = new RouteNode(airportModel, null, 0l, 0l, DistanceHelper.calculateDistance(airportModel, endPoint), 0l);
            openSet.add(routeNode);
            allNodes.put(airportModel, routeNode);
        }
    }

    public CheapestRoutesResult execute(){
        Double bestEndPrice = Double.POSITIVE_INFINITY;
        Double bestEndDistance = Double.POSITIVE_INFINITY;
        RouteNode bestEndRouteNode = null;
        while (!openSet.isEmpty()) {
            RouteNode next = openSet.poll();

            if (next.getCurrent().getCity().getId().equals(destinationCityId)) {
                if(next.getTotalRoutePrice() < bestEndPrice || (next.getTotalRoutePrice() == bestEndPrice && next.getTotalDistance() < bestEndDistance)) {
                    bestEndRouteNode = next;
                    bestEndPrice = bestEndRouteNode.getTotalRoutePrice();
                    bestEndDistance = next.getTotalDistance();
                    continue;
                }
            }

            if(next.getTotalRoutePrice() >= bestEndDistance){
                continue;
            }

            findNextRouteOptions(next);
        }

        return getResult(bestEndRouteNode);
    }

    private void findNextRouteOptions(RouteNode currentNode){
        currentNode.getCurrent().getRoutes().forEach(routeModel -> {
            AirportModel destinationNode = nodesPerId.get(routeModel.getDestinationId());

            RouteNode nextNode = allNodes.getOrDefault(destinationNode, new RouteNode(destinationNode));
            double newPrice = currentNode.getTotalRoutePrice() + routeModel.getPrice();
            allNodes.put(destinationNode, nextNode);

            if (newPrice < nextNode.getTotalRoutePrice()) {
                nextNode.setPrevious(currentNode.getCurrent());
                nextNode.setTotalRoutePrice(newPrice);
                nextNode.setEstimatedScore(DistanceHelper.calculateDistance(destinationNode, endPoint));
                nextNode.setTotalDistance(nextNode.getTotalDistance() + routeModel.getDistance());
                nextNode.setCurrentRoutePrice(routeModel.getPrice());
                openSet.add(nextNode);
            }
        });
    }

    private CheapestRoutesResult getResult(RouteNode bestEndRouteNode){
        CheapestRoutesResult result;

        if(bestEndRouteNode != null){
            List<RouteDto> routes = new ArrayList<>();
            RouteNode current = bestEndRouteNode;
            do {
                if(current.getPrevious() != null) {
                    routes.add(0, new RouteDto(current));
                }

                current = allNodes.get(current.getPrevious());
            } while (current != null);


            result =  new CheapestRoutesResult(routes, FormatHelper.roundTo2Decimals(bestEndRouteNode.getTotalRoutePrice()),
                    String.format("%s km", FormatHelper.roundTo2Decimals(bestEndRouteNode.getTotalDistance())));
        } else {
            result = new CheapestRoutesResult(List.of(), null, null);
        }

        return result;
    }
}
