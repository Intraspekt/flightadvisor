package htec.com.service.calcEngine;

import htec.com.bom.entities.Airport;
import htec.com.bom.entities.Route;
import htec.com.service.dtos.CheapestRoutesResult;

import java.util.List;

public interface Graph {
    void initializeNodes(List<Airport> airports, List<Route> routes);
    CheapestRoutesResult findCheapestRoutes(long sourceCityId, long destinationCityId);
}
