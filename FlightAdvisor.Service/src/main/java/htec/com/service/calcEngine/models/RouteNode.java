package htec.com.service.calcEngine.models;

import lombok.Data;

@Data
public class RouteNode implements Comparable<RouteNode> {
    private final AirportModel current;
    private AirportModel previous;
    private double totalRoutePrice;
    private double totalDistance;
    private double estimatedScore;
    private double currentRoutePrice;

    public RouteNode(AirportModel current) {
        this(current, null, Double.POSITIVE_INFINITY, 0l, Double.POSITIVE_INFINITY, 0l);
    }

    public RouteNode(AirportModel current, AirportModel previous, double totalRoutePrice, double totalDistance, double estimatedScore, double currentRoutePrice) {
        this.current = current;
        this.previous = previous;
        this.totalRoutePrice = totalRoutePrice;
        this.estimatedScore = estimatedScore;
        this.totalDistance = totalDistance;
        this.currentRoutePrice = currentRoutePrice;
    }

    @Override
    public int compareTo(RouteNode other) {
        if (this.estimatedScore > other.estimatedScore) {
            return 1;
        } else if (this.estimatedScore < other.estimatedScore) {
            return -1;
        } else {
            return 0;
        }
    }
}
