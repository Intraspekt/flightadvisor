package htec.com.api.infrastructure.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ApiError {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String type;
    private List<String> errorCodes;

    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(Throwable ex) {
        this();
        type = ErrorTypeEnum.general.toString();
        errorCodes = List.of("error.unexpected");
    }

    public ApiError(ErrorTypeEnum errorType, String messageCode) {
        this();
        type = errorType.toString();
        errorCodes = List.of(messageCode);
    }

    public ApiError(ErrorTypeEnum errorType, List<String> messageCodes) {
        this();
        type = errorType.toString();
        errorCodes = messageCodes;
    }
}