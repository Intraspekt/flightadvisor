package htec.com.api.infrastructure.exceptions;

public enum ErrorTypeEnum {
    validation,
    service,
    general
}
