package htec.com.api.controllers;

import htec.com.service.services.interfaces.AirportService;
import htec.com.service.services.interfaces.RouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("import")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class ImportController {
    private final AirportService airportService;
    private final RouteService routeService;

    @PostMapping("/airports")
    public ResponseEntity importAirports(){
        airportService.importData();

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/routes")
    public ResponseEntity importRoutes(){
        routeService.importData();

        return new ResponseEntity(HttpStatus.OK);
    }
}
