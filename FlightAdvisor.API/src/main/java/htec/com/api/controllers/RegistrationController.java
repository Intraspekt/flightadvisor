package htec.com.api.controllers;

import htec.com.service.dtos.UserDto;
import htec.com.service.services.interfaces.RegistrationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
public class RegistrationController {

    private final RegistrationService registrationService;

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody @Valid UserDto user){
        registrationService.registerUser(user);

        return new ResponseEntity(HttpStatus.OK);
    }
}
