package htec.com.api.controllers;

import htec.com.bom.entities.City;
import htec.com.service.dtos.CommentDto;
import htec.com.service.services.interfaces.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("comments")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('BASIC_USER')")
public class CommentsController {
    private final CommentService commentService;

    @PostMapping()
    public ResponseEntity<CommentDto> create(@RequestBody @Valid CommentDto comment){
        CommentDto savedComment = commentService.create(comment);

        return new ResponseEntity<>(savedComment, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CommentDto> edit(@PathVariable Long id, @RequestBody @Valid CommentDto comment){
        CommentDto savedComment = commentService.edit(id, comment);

        return  new ResponseEntity<>(savedComment, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        commentService.delete(id);

        return  new ResponseEntity<>(HttpStatus.OK);
    }
}
