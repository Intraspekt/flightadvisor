package htec.com.api.controllers;

import htec.com.service.dtos.CheapestRouteRequest;
import htec.com.service.dtos.CheapestRoutesResult;
import htec.com.service.services.interfaces.RouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("routes")
@RequiredArgsConstructor
public class RoutesController {
    private final RouteService routeService;

    @GetMapping()
    @PreAuthorize("hasAuthority('BASIC_USER')")
    public ResponseEntity<CheapestRoutesResult> getCheapestRoutes(@Valid CheapestRouteRequest requestModel){
        CheapestRoutesResult result = routeService.findCheapestRoutes(requestModel.getSourceCityId(), requestModel.getDestinationCityId());

        return result.getRoutes().isEmpty() ? new ResponseEntity(result, HttpStatus.NO_CONTENT) : new ResponseEntity(result, HttpStatus.OK);
    }
}
