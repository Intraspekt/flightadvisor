package htec.com.api.controllers;

import htec.com.service.dtos.CityDto;
import htec.com.service.services.interfaces.CityService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("cities")
@RequiredArgsConstructor
public class CityController {
    private final CityService cityService;

    @GetMapping
    @PreAuthorize("hasAuthority('BASIC_USER')")
    public ResponseEntity<List<CityDto>> getAll(@RequestParam(required = false) String name, @RequestParam(required = false) Integer numberOfComments){
        List<CityDto> cities = cityService.get(name, numberOfComments);

        return new ResponseEntity<>(cities, CollectionUtils.isEmpty(cities) ? HttpStatus.NO_CONTENT : HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<CityDto> create(@RequestBody @Valid CityDto city){
         return  new ResponseEntity<>(cityService.create(city), HttpStatus.OK);
    }
}
