package htec.com.api.security;

public class SecurityConstants {
    public static final String AUTH_LOGIN_URL = "/login";

    // Signing key for HS512 algorithm
    public static final String JWT_SECRET = "n2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRf";

    // JWT token defaults
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "htec-FAI";
    public static final String TOKEN_AUDIENCE = "htec-FAA";
    public static final Long TOKEN_EXPIRATION_TIME = 3600000L;
    public static final String TOKEN_ROLE_CLAIM = "rol";
}
