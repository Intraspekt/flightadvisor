package htec.com.bom.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Airport {
    @Id
    private Long id;
    private String name;

    @ManyToOne
    @JoinColumn(name="refCity", nullable=false)
    private City city;

    private String country;
    private String IATA;
    private String ICAO;
    private Double latitude;
    private Double longitude;
    private Double atitude;
    private Double utcOffset;
    private Character DST;
    private String timeZone;
    private String type;
    private String source;

//    @OneToMany(mappedBy = "source", targetEntity=Route.class,cascade = CascadeType.ALL , fetch = FetchType.LAZY)//Route.class, cascade = CascadeType.ALL, orphanRemoval = true)
//    //@JoinColumn(referencedColumnName="refSourceAirport", nullable=false, table = "route")
//    private List<Route> routes;

//    @OneToMany(mappedBy = "route", fetch = FetchType.EAGER)//Route.class, cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(referencedColumnName="refSourceAirport", table = "route")
//    private Set<Route> destinationRoutes;
}
