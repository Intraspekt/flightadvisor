package htec.com.bom.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity {

    private String firstName;
    private String lastName;
    private String username;
    private String password;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name="relationUserUserRole",
            joinColumns = {@JoinColumn(name="refUser", referencedColumnName="id")},
            inverseJoinColumns = {@JoinColumn(name="refUserRole", referencedColumnName="id")}
            )
    private Set<UserRole> userRoles;
}
