package htec.com.bom.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class City {
    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String description;
    private String country;

    @OneToMany
    @JoinColumn(name = "refCity")
    @OrderBy("updatedOn desc")
    private List<Comment> comments;
}
