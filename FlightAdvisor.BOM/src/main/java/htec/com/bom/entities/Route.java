package htec.com.bom.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Route extends BaseEntity {

    private String airlineCode;
    private Long airlineId;

    @ManyToOne
    @JoinColumn(name = "refSourceAirport")
    private Airport source;

    @ManyToOne
    @JoinColumn(name = "refDestinationAirport")
    private Airport destination;

    private boolean codeshare;
    private Integer numberOfStops;
    private String equipment;
    private Double price;
}
