package htec.com.dal.repositories;

import htec.com.bom.entities.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findByName(String name);
    Boolean existsByName(String name);
}
