package htec.com.dal.repositories;

import htec.com.bom.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsernameIgnoreCase(String username);
    User findByUsername(String username);
    Boolean existsByUsername(String username);
}
