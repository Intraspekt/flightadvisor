package htec.com.dal.repositories;

import htec.com.bom.entities.Airport;
import htec.com.bom.entities.Route;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RouteRepository extends JpaRepository<Route, Long> {
}
